 Foxhole Global V2.2
====================

> This is a fork of https://github.com/muloneweb/FHGHQ

[[_TOC_]]

## Sites

**Production**

- War 1 server: https://live-1.foxehole-hq.com/auth
- War 2 server: https://live-2.foxehole-hq.com/auth

**Development / Beta**

> This is NOT PERSISTENT
- War 2 server: https://dev.foxehole-hq.com/auth

## Deployment

- `FHGHQ_URL`: must be `http://{your_domain.com}`
- An instance of FHGHQ can only target **1 Forehole war map at a time**.

> Please note by default this targets the WAR 2 foxehole server. See [configuration](#configuration)


### Using docker

**Without persistence**
```bash
docker run -p 3000:3000 registry.gitlab.com/a.couty/fhghq:master
```

Go to http://localhost:3000

### Using helm

```bash
$ kubectl create namespace fhghq
$ kubectl config set-context --namespace fhghq 
$ helm repo add
$ helm upgrade --install --namespace fhghq fhghg .
```

## Development

- node v15.x

**Single build & run**
```bash
$ npm install
$ npm run-script build
$ npm start
```

**Build on change**
```bash
$ npm install
$ npm run-script dev
```

Go to http://localhost:3000

## Configuration

Can be done via two ways:
1. Editing [config.js](conf/config.js)
2. Setting environment variables. Environment variables should names should be in CAPS, replace "." with "_".
    For example, "logger.level" can be overriden with LOGGER_LEVEL

| config | description | default |
| ------ | ------ | ------ |
| logger.level | Log level | info |
| fhghq.url | Url to hit the server | http://localhost:3000 |
| steamApi.key | [Steam API access key](https://steamcommunity.com/dev/apikey) | '' |
| steamApi.foxeHoleStatsUrl | false | https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid=505460 |
| warApi.liveUrl | false | https://war-service-live-2.foxholeservices.com |
| warApi.statsUpdateInternal | false | 60000 |
| warApi.mapUpdateInterval | false | 60000 |
| discord.token | true | '' |
| sqlLite.fileNameGiven | false | './.data/FHGHQ.db' |

## Repository Structure

```
.
├── 0.26 Town List.md          # ?
├── conf                       # Configuration files
├── Dockerfile                 # Docker file
├── helm                       # Helm chart (overkill)
├── package.json               # Dependencies
├── README.md                  # This file
├── server                     # Bakckend application files
├── src                        # Frontend files (served as static)
├── TO-DO.md                   # ?
├── views                      # HTML view
└── webpack.config.js          # Frontend webpack configuration file
```

_________
Shortcuts:

[MAP LIST](https://war-service-live.foxholeservices.com/api/worldconquest/maps/) https://war-service-live.foxholeservices.com/api/worldconquest/maps/

[WAR INFO](https://war-service-live.foxholeservices.com/api/worldconquest/war) https://war-service-live.foxholeservices.com/api/worldconquest/war

[DYNAMIC](https://war-service-live.foxholeservices.com/api/worldconquest/maps/DeadLandsHex/dynamic/public) https://war-service-live.foxholeservices.com/api/worldconquest/maps/DeadLandsHex/dynamic/public

[STATIC](https://war-service-live.foxholeservices.com/api/worldconquest/maps/DeadLandsHex/static) https://war-service-live.foxholeservices.com/api/worldconquest/maps/DeadLandsHex/static

[WAR REPORT](https://war-service-live.foxholeservices.com/api/worldconquest/warReport/DeadLandsHex) https://war-service-live.foxholeservices.com/api/worldconquest/warReport/DeadLandsHex
