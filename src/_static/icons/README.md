All icons are taken from [Foxehole game](https://www.foxholegame.com/). 

## Updating icons list

**DOT NOT DELETE EXISTING FOLDERS, JUST REPLACE THE CONTENT**

1. Download [UE Viewer](https://www.gildor.org/downloads)
2. Open game files, unpack, export as PNG
3. Go into `UmodelExport/Textures/UI`
3. Copy / paste `ItemIcons`, `MapIcons` and `VehicleIcons` directories into `src/_static/icons`





