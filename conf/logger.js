
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const conf = require('./config')

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} [${level}]: ${message}`;
});

const logger = createLogger({
  level: conf.logger.level,
  format: combine(
    format.colorize(),
    timestamp(),
    myFormat
  ),
  transports: [
    new (transports.Console)({'timestamp':true}),
  ],
});

module.exports = logger
