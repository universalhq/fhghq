const env = require('common-env')()

const config = env.getOrElseAll({
  logger: {
    level: 'info'
  },
  fhghq: {
    url: 'http://localhost:3000'
  },
  steamApi: {
    key: '',
    foxeHoleStatsUrl: 'https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid=505460'
  },
  warApi: {
    liveUrl: 'https://war-service-live-2.foxholeservices.com',
    statsUpdateInternal: 60000,
    mapUpdateInterval: 60000
  },
  discord: {
    token: ''
  },
  sqlLite: {
    fileNameGiven : {
      $default: './.data/FHGHQ.db'
    }
  }
})

module.exports = config
