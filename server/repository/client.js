const fs = require('fs')
const path = require('path')
const SQLite = require('better-sqlite3')

const conf = require('../../conf/config')
const logger = require('../../conf/logger')

// create data directory if does not exist
const databaseDir = path.basename(path.dirname(conf.sqlLite.fileNameGiven))
if (!fs.existsSync(databaseDir)) {
  logger.debug(`Data directory ${databaseDir} does not exists, creating ...`)
  fs.mkdirSync(databaseDir)
}

/**
 * Singleton pattern allowing to share only 1 instance of SQLite client
 * @returns {*}
 */
let sql
module.exports = () => {
  if (!sql) sql = SQLite(conf.sqlLite.fileNameGiven, {})
  return sql
}
