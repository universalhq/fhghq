/**
 * Events repository
 */
const sqlClient = require('./client')()

/**
 *
 * @param warId: number
 * @returns {*}
 */
exports.getWarById = (warId) => {
  return sqlClient.prepare('SELECT * FROM warhistory WHERE warnumber = ?').get(warId)
}
