const shortid = require('shortid')
const { createUser }  = require('../repository/user.repository')

exports.handleAuth = (req, res) => {
  const userId = 'anonymous' + shortid.generate().slice(0, 8) // generated user id
  const salt = shortid.generate()

  createUser({
    id: userId,
    salt,
    name: req.query.name,
    avatar: new Date().toString()
  })

  res.cookie('steamid', userId);
  res.cookie('salt', salt);
  if (!req.headers['cookie'] || !req.headers['cookie'].includes('redir_room')) {
    res.send({ redir: false });
  } else {
    const cookiestring = req.headers['cookie'];
    const id = cookiestring.substring(cookiestring.indexOf(' redir_room') + 12, cookiestring.indexOf(' redir_room') + 21);
    res.send({
      redir: true,
      redirId: id
    });
    //res.redirect('/room/'+id);
  }
}
