const userService = require('../services/user.service')

/**
 * check if a user is logged by checking cookie & password in the db
 * @param req HTTPRequest
 * @returns {boolean}
 */
exports.isLoggedIn = (req) => {
  if (!req.headers.cookie || !req.headers.cookie.includes('salt')) return false

  const cookiestring = req.headers.cookie;
  const id = cookiestring.substring(cookiestring.indexOf(' steamid=') + 9, cookiestring.indexOf(' steamid=') + 26).replace(';', '');
  let salt = cookiestring.substring(cookiestring.indexOf(' salt=') + 6, cookiestring.indexOf(' salt=') + 15).replace(';', '');
  const user = userService.getUserById(id);
  if (!user) return false

  if (id === user.id && salt.slice(0, 9) === user.salt.slice(0, 9)) return true

}

/**
 * Checks a user is authenticated.
 *
 * Looks a bit over complicated imo
 * @param req Http Request made by the user
 * @param res Http Response made by the user
 * @param next Next function to go through after auth has been checked
 * @returns {boolean}
 */
exports.checkAuth = (req, res, next) => {
  if (!exports.isLoggedIn(req)) {
    if (req.originalUrl.search('/room/') !== -1) {
      const roomId = req.originalUrl.replace("/room/", "")
      res.cookie('redir_room', roomId)
    }
    return res.redirect('/auth')
  } else {
    next()
  }

  // if (!req.headers.cookie || !req.headers.cookie.includes('salt')) {
  //   return res.redirect('/auth')
  // }
  // const cookiestring = req.headers.cookie;
  // const id = cookiestring.substring(cookiestring.indexOf(' steamid=') + 9, cookiestring.indexOf(' steamid=') + 26).replace(';', '');
  // let salt = cookiestring.substring(cookiestring.indexOf(' salt=') + 6, cookiestring.indexOf(' salt=') + 15).replace(';', '');
  // const user = userService.getUserById(id);
  //
  // if (!user) {
  //   // Handle the case when someone clicked on  a roomURl but is not logged in.
  //   // we inject the "redir_room" with the roomId in the cookies so that when he logs-in we know
  //   // where to redirect the user : to the room he wanted to access !
  //   if (req.originalUrl.search('/room/') !== -1) {
  //     const roomId = req.originalUrl.replace("/room/", "")
  //     res.cookie('redir_room', roomId)
  //   }
  //   return res.redirect('/auth')
  // } else if (id === user.id && salt.slice(0, 9) === user.salt.slice(0, 9)) {
  //   // The user is authenticated, we go to the next function that usually takes care of the actual request
  //   next()
  // }
}
