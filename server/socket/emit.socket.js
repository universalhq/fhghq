const io = require('./client')()

const logger = require('../../conf/logger')

/**
 * @param roomId : string
 */
exports.deleteRoom = (roomId) => {
  io.to(roomId)
    .emit('deleteroom');
};

/**
 *
 * User parameter comes from userService.getRequester
 *
 * @param roomId : string
 * @param user : object
 */
exports.accessRequest = (roomId, user) => {
  io.to(roomId)
    .emit('requestaccess', user);
}


/**
 *
 * @param dynamicData
 * @param eventsData
 */
exports.updateDynamicMap = (dynamicData, eventsData) => {
  io.emit('updatedynmap', {
    dynamic: dynamicData,
    events: eventsData
  });
  logger.info('Map updated.');
}

/**
 *
 * @param totalPlayers: number
 * @param warStats
 * @param warReport
 */
exports.updateStats = (totalPlayers, warStats, warReport) => {
  const stats = {
    totalPlayers,
    warstats: warStats,
    wr: warReport,
    currentWar: undefined // comes from foxeholeglobal, see getFoxeholeGlobalStats deprecated function
  }
  io.to('stats')
    .emit('updatestats', stats);
};

/**
 * @param roomId : string
 * @param userId : string
 */
exports.leaveRoom = function (userId, roomId) {
  io.to(roomId)
    .emit('leaveroomroom', userId);
};

