const fetch = require('node-fetch')

const conf = require('../../conf/config')
const logger = require('../../conf/logger')
const warApi = require('../warapi/warapi')
const warApiStaticDataService = require('../services/warapi.static.service')
const emitSocket = require('../socket/emit.socket')


exports.totalPlayers = 0
exports.warstats = {}
let warReport = {};
exports.warReport = warReport

/**
 * Get foxehole steam foxehole statistics and set exports.totalplayers
 * @returns {Promise<void>}
 */
exports.getSteamTotalPlayers = async () => {
  return fetch(conf.steamApi.foxeHoleStatsUrl, { method: 'get'})
    .then(response => { return response.json(); })
    .then(jsonResponse => {
      logger.info(`Got ${jsonResponse.response.player_count} players from steam stats ${conf.steamApi.foxeHoleStatsUrl}`)
      return jsonResponse.response.player_count
    })
    .catch(reason => logger.error(`Could not fetch map list from ${conf.steamApi.foxeHoleStatsUrl}: ${reason}`))
}

/**
 * Update statistics
 * @returns {Promise<void>}
 */
exports.updateStats = async () => {
  // set exports.totalPalyers
  await exports.getSteamTotalPlayers()
    .then(totalPlayers => { exports.totalPlayers = totalPlayers})
  // set exports.warStats
  await warApi.getWarStats()
    .then(warStats => { exports.warStats = warStats})
  logger.info('Updated stats')
  // exports.getFoxeholeGlobalStats()
  await exports.updateWarReport()
}

exports.updateWarReport = async () => {
  let startUpdateDate = new Date();
  const regionNames = await warApiStaticDataService.getRegionNames()
  const updateWarStatsPromises = regionNames.map(async (regionName) => {
      exports.warReport[regionName] = await warApi.getMapStats(regionName)
    })
  await Promise.all(updateWarStatsPromises)
  logger.info('War report API update successful, all regions updated');
  let finishLoadDate = new Date();
  let timedif = Math.abs(finishLoadDate - startUpdateDate) / 1000;
  logger.info(timedif + ' seconds to load data');
  emitSocket.updateStats(exports.totalPlayers, exports.warStats, exports.warReport)
}

/**
 * ! deprecated method
 * Taken from socket.js, this is broken because foxholeglobal.com is down
 * @returns {Promise<void>}
 */
exports.getFoxeholeGlobalStats = async () => {
  var request2 = new XMLHttpRequest();
  request2.responseType = 'json';
  request2.open('GET',  'http://foxholeglobal.com/service/currentWar', true);
  request2.onload = function () {
    if (request2.status >= 200 && request2.status < 400) {
      try {
        var data = this.response;
        exports.currentwar = data;
        let savedwars = sql.prepare('SELECT warnumber FROM warhistory;')
          .all();
        logger.info('Saved wars');
        logger.info(savedwars);
        for (let i = 0; i < data.warstats.length - 1; i++) {
          let present = false;
          for (let j = 0; j < savedwars.length; j++) {
            if (data.warstats[i].warNumber === savedwars[j].warnumber) {
              present = true;
            }
          }
          if (!present) {
            var requestwar = new XMLHttpRequest();
            requestwar.responseType = 'json';
            requestwar.open('GET', 'http://foxholeglobal.com/service/prevWar/' + data.warstats[i].warNumber, true);
            requestwar.onload = function () {
              if (requestwar.status >= 200 && requestwar.status < 400) {
                try {
                  let wardata = this.response;
                  const insertwar = sql.prepare('INSERT INTO warhistory (warnumber, events, reports, warstats,startpoint) VALUES (@warnumber, @events, @reports, @warstats, @startpoint);');
                  let e = {
                    warnumber: data.warstats[i].warNumber,
                    events: JSON.stringify(wardata.events),
                    reports: JSON.stringify(wardata.reports),
                    warstats: JSON.stringify(wardata.warStats),
                    startpoint: JSON.stringify(wardata.startpoint)
                  };
                  insertwar.run(e);
                } catch (err) {
                }
              }
            };
            requestwar.send();
          }
        }
      } catch (err) {
        logger.info(err);
      }
    }
  };
  request2.send();
}
